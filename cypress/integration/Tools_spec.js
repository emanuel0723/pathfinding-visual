describe('App Controls', () => {
  it('creates a node wall', () => {
    // Given
    cy.visit('/')

    // When
    cy.get('input[value=wallTool]').click()
    cy.get('#node-0-0').click()

    // Then
    cy.get('#node-0-0').should('have.class', 'node-wall')
  })

  it('cleans a node wall', () => {
    // Given
    cy.visit('/')
    cy.get('#node-0-0').click()

    // When
    cy.get('input[value=cleanWallTool]').click()
    cy.get('#node-0-0').click()

    // Then
    cy.get('#node-0-0').should('not.have.class', 'node-wall')
  })

  it('enables weighted grid', () => {
    // Given
    cy.visit('/')

    // When
    cy.get('input[value=weightedGrid]').click()

    // Then
    cy.get('input[value=weightTool]').should('be.enabled')
  })

  it('sets the weight of a node', () => {
    // Given
    cy.visit('/')

    // When
    cy.get('input[value=weightedGrid]').click()
    cy.get('input[value=weightTool]').click()
    cy.get('#input-node-0-0').click()

    // Then
    cy.get('#input-node-0-0').clear().type(7)
    cy.get('#HeaderAppControls').click()
    cy.get('#input-node-0-0').invoke('val').should('eq', '7');
  })
})
