describe('Algorithms', () => {
  it('runs bfs algorithm', () => {
    // Given
    cy.visit('/')

    // When
    cy.get('#AlgoBtn_bfs').click()

    // Then
    cy.get('#StatisticsPanel').should('be.visible')
  })

  it('runs dfs algorithm', () => {
    // Given
    cy.visit('/')

    // When
    cy.get('#AlgoBtn_dfs').click()

    // Then
    cy.get('#StatisticsPanel').should('be.visible')
  })

  it('runs bi-bfs algorithm', () => {
    // Given
    cy.visit('/')

    // When
    cy.get('#AlgoBtn_bi-bfs').click()

    // Then
    cy.get('#StatisticsPanel').should('be.visible')
  })
})