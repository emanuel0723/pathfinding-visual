describe('App Controls', () => {
  it('validates initial state', () => {
    // Given
    cy.visit('/')

    // When
    // Then
    cy.contains('Path Finding Visualizer')
    cy.get('#ControlsBar').should('be.visible')
    cy.get('#Content').should('be.visible')
    cy.get('#ResetBtn').should('be.enabled')
    cy.get('#CleanBtn').should('be.disabled')

    cy.get('input[value=wallTool]').should('be.checked')
    cy.get('input[value=cleanWallTool]').should('not.be.checked')
    cy.get('input[value=weightTool]').should('not.be.checked')
    cy.get('input[value=weightTool]').should('be.disabled')

    cy.get('.node-start').should('have.lengthOf', 1)
    cy.get('.node-end').should('have.lengthOf', 1)
  })

  it('resets the board after algorithm ran and walls created', () => {
    // Given
    cy.visit('/')

    // When
    for (let i = 0; i < 5; i++) {
      cy.get('#node-0-' + i).click()
    }
    cy.get('#AlgoBtn_bfs').click()
    //cy.wait(3000)

    cy.get('#ResetBtn').click()

    // Then
    cy.get('.node').each(($el, index, $list) => {
      cy.wrap($el).should('not.have.class', 'node-wall')
      cy.wrap($el).should('not.have.class', 'node-visited')
      cy.wrap($el).should('not.have.class', 'node-winner')
    })
  })

  it('cleans the board after algorithm ran and walls created', () => {
    // Given
    cy.visit('/')

    // When
    for (let i = 0; i < 5; i++) {
      cy.get('#node-0-' + i).click()
    }
    cy.get('#AlgoBtn_bfs').click()
    //cy.wait(3000)

    cy.get('#CleanBtn').click()

    // Then
    cy.get('.node').each(($el, index, $list) => {
      cy.wrap($el).should('not.have.class', 'node-visited')
      cy.wrap($el).should('not.have.class', 'node-winner')
    })

    for (let i = 0; i < 5; i++) {
      cy.get('#node-0-' + i).should('have.class', 'node-wall')
    }
  })
})
