import { bfs } from '../Algorithms/bfs.js'
import { dfs } from '../Algorithms/dfs.js'
import { bibfs } from '../Algorithms/bibfs.js'
import { dijkstra } from '../Algorithms/dijkstra.js'
import { DEBUG_LEVEL } from './PathfinderVisualizer'

let timeoutId

/* ******** Breadth First Search ********** */
export function visualizeBFS(grid, startNode, endNode, callback=() => {}) {
  const visitedNodesInOrder = bfs(grid, startNode, endNode)
  const pathLength = animateSearch(visitedNodesInOrder, startNode, endNode, callback)
  return [visitedNodesInOrder.length, pathLength]
}

/* ******** Depth First Search ********** */
export function visualizeDFS(grid, startNode, endNode, callback=() => {}) {
  const visitedNodesInOrder = dfs(grid, startNode, endNode)
  const pathLength = animateSearch(visitedNodesInOrder, startNode, endNode, callback)
  return [visitedNodesInOrder.length, pathLength]
}

/* ******** Bidirectional Breadth First Search ********** */
export function visualizeBiBFS(grid, startNode, endNode, callback=() => {}) {
  const result = bibfs(grid, startNode, endNode)
  const visitedNodesInOrder = result[0]
  const collision1 = result[1]
  const collision2 = result[2]
  animateSearch(visitedNodesInOrder, startNode, endNode, callback)
  if (collision1 && collision2) {
    let n = visitedNodesInOrder.length
    const path1 = createPath(collision1)
    const path2 = createPath(collision2)
    setTimeout(() => {
      animatePath(path1, endNode, callback)
      animatePath(path2, endNode, callback)
    }, 10 * n)
    return [visitedNodesInOrder.length, path1.length + path2.length + 1]
  }
}

/* ******** Dijkstra ********** */
export function visualizeDijkstra(grid, startNode, endNode, callback=() => {}) {
  const visitedNodesInOrder = dijkstra(grid, startNode, endNode)
  const pathLength = animateSearch(visitedNodesInOrder, startNode, endNode, callback)
  return [visitedNodesInOrder.length, pathLength]
}

/**
 * Function to clear any pending timeout in case still drawing
 */
export function clearTimeouts() {
  while (timeoutId) {
    window.clearTimeout(timeoutId)
    timeoutId--
  }
}

/* ******** Generic functions ********** */
function createPath(endNode) {
  let path = []
  let curr = endNode

  while (curr.parentNode != null) {
    path.push(curr)
    curr = curr.parentNode
  }
  return path
}

function animateSearch(visitedNodesInOrder, start, endNode, callback=() => {}) {
  let n = visitedNodesInOrder.length
  let path = createPath(endNode)
  for (let i = 0; i < n; i++) {
    timeoutId = setTimeout(() => {
      const node = visitedNodesInOrder[i]
      if (node.row === endNode.row && node.col === endNode.col) {
        animatePath(path, endNode, callback)
        return
      }

      if (node.row !== start.row || node.col !== start.col) {
        document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-visited'

        if (DEBUG_LEVEL > 1) {
          let nodeHtml = document.getElementById(`node-${node.row}-${node.col}`)
          let htmlTemp = nodeHtml.innerHTML
          nodeHtml.innerHTML = htmlTemp + '<br/>' + node.isVisited
        }
      }
    }, 10 * parseInt(i))
  }
  return path.length
}

function animatePath(path, endNode, callback=() => {}) {
  let n = path.length
  for (let i = n - 1; i >= 0; i--) {
    timeoutId = setTimeout(() => {
      if (path[i] !== endNode) {
        document.getElementById(`node-${path[i].row}-${path[i].col}`).className = 'node node-winner'
      }
    }, 20 * (n - i))
  }

  // Callback after timeouts
  timeoutId = setTimeout(() => {
    callback()
  }, 20 * n) 
}
