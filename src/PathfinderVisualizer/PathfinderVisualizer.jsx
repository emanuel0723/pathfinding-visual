import React, { Component } from 'react'
import AppControls from './Components/AppControls'
import Grid from './Components/Grid'
import Tools, { wallTool, cleanWallTool, weightTool } from './Components/Tools'
import * as controller from './PathfinderController.js'

import '../styles/myStyle.css'
import 'react-rangeslider/lib/index.css'

export const DEBUG_LEVEL = 0
const MIN_COLUMNS = 7
const MAX_COLUMNS = 50
const NUMBER_COLUMNS = 20
const algorithms = [
  {
    'name': 'bfs',
    'description': 'Breadth First Search',
    'function': controller.visualizeBFS,
  },
  {
    'name': 'dfs',
    'description': 'Depth First Search',
    'function': controller.visualizeDFS,
  },
  {
    'name': 'bi-bfs',
    'description': 'Bidirectional BFS',
    'function': controller.visualizeBiBFS,
  },
  {
    'name': 'dijkstra',
    'description': 'Dijkstra',
    'function': controller.visualizeDijkstra,
  },
]

let mouseIsPressed = false
let movingStart = false
let movingEnd = false
let running = null

export default class PathfinderVisualizer extends Component {
  constructor(props) {
    super(props)

    this.componentDidMount = this.componentDidMount.bind(this)
    this.visualizeAlgorithm = this.visualizeAlgorithm.bind(this)
    this.handleMouseDownGrid = this.handleMouseDownGrid.bind(this)
    this.handleMouseUp = this.handleMouseUp.bind(this)
    this.handleMouseEnter = this.handleMouseEnter.bind(this)
    this.sizeAndCreateGrid = this.sizeAndCreateGrid.bind(this)
    this.resetGrid = this.resetGrid.bind(this)
    this.cleanGrid = this.cleanGrid.bind(this)
    this.onToolChange = this.onToolChange.bind(this)
    this.onWeightedGridChange = this.onWeightedGridChange.bind(this)
    this.changeNodeParams = this.changeNodeParams.bind(this)

    this.appControlsElement = React.createRef()

    this.state = {
      grid: [],
      startNode: null,
      endNode: null,
      numberColumns: NUMBER_COLUMNS,
      selectedTool: wallTool,
      weightedGrid: false,
      algoFinished: false,
      algoResults: {
        totalNodes: 0,
        nodesVisited: 0,
        pathLength: 0
      }
    }
  }

  componentDidMount() {
    // window width - sidebar width - gridContainer margin*2 - gridContainer border*2
    const gridWidth = window.innerWidth - 180 - 40 - 20
    // window height - header height - gridContainer margin*2 - gridContainer border*2
    const gridHeight = window.innerHeight - 150 - 40 - 20
    const nodeSize = gridWidth / NUMBER_COLUMNS
    const rows = Math.floor(gridHeight / nodeSize)
    const cols = NUMBER_COLUMNS
    const startNodeRow = Math.floor(rows / 2)
    const startNodeCol = Math.floor(cols * 0.2)
    const endNodeRow = Math.floor(rows / 2)
    const endNodeCol = Math.floor(cols * 0.7)
    const initialGrid = getInitialGrid(rows, cols, startNodeRow, startNodeCol, endNodeRow, endNodeCol)
    this.appControlsElement.current.updateControls(null)
    this.setState({
      grid: initialGrid,
      startNode: initialGrid[startNodeRow][startNodeCol],
      endNode: initialGrid[endNodeRow][endNodeCol],
      nodeSize: nodeSize,
      numberColumns: NUMBER_COLUMNS,
    })
  }

  resetGrid() {
    const { grid, startNode, endNode } = this.state
    const defaultRgb = [232, 245, 253]

    grid.forEach((row) => {
      row.forEach((node) => {
        if (node !== startNode && node !== endNode) {
          document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-clean'
          if (this.state.weightedGrid && !node.isWall) {
            document.getElementById(`input-node-${node.row}-${node.col}`).value = 0
            document.getElementById(`input-node-${node.row}-${node.col}`).style.background = 'rgb(' + defaultRgb[0] + ',' + defaultRgb[1] + ',' + defaultRgb[2] + ')'
          }
        }
        node.isVisited = false
        node.isWall = false
        node.parentNode = null
        node.weight = 0
      })
    })

    running = null
    this.appControlsElement.current.updateControls(null)
    controller.clearTimeouts()
    this.setState({ algoFinished: false })
  }

  cleanGrid() {
    const { grid, startNode, endNode } = this.state

    grid.forEach((row) => {
      row.forEach((node) => {
        if (node !== startNode && node !== endNode && !node.isWall) {
          document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-clean'
        }
        node.isVisited = false
        node.parentNode = null
      })
    })

    running = null
    this.appControlsElement.current.updateControls(null)
    controller.clearTimeouts()
    this.setState({ algoFinished: false })
  }

  onToolChange(event) {
    this.setState({ selectedTool: event.target.value })
  }

  onWeightedGridChange() {
    this.setState({ weightedGrid: !this.state.weightedGrid })
  }

  visualizeAlgorithm(algo) {
    running = algo.name
    const { grid, startNode, endNode } = this.state
    this.setState({ algoFinished: false })

    const count = algo.function(grid, startNode, endNode, this.setAlgoFinished.bind(this))
    this.setState({ algoResults: {
        totalNodes: grid.length * grid[0].length,
        nodesVisited: count[0],
        pathLength: count[1],
        algorithm: algo.description
      }
    })


    this.appControlsElement.current.updateControls(algo.name)
  }

  setAlgoFinished() {
    this.setState({ algoFinished: true })
  }

  render() {
    const { grid, mouseIsPressed, nodeSize, numberColumns, selectedTool, weightedGrid, startNode, endNode } = this.state

    if (startNode && endNode) {
      let startNodeDom = document.getElementById(`node-${startNode.row}-${startNode.col}`)
      let endNodeDom = document.getElementById(`node-${endNode.row}-${endNode.col}`)
      if (startNodeDom && endNodeDom) {
        startNodeDom.style.fontSize = nodeSize + 'px'
        endNodeDom.style.fontSize = nodeSize + 'px'
      }
    }

    return (
      <React.Fragment>
        <header id='HeaderAppControls'>
          <link href='https://fonts.googleapis.com/css?family=Quicksand:300,500' rel='stylesheet'></link>
          <AppControls
            ref={this.appControlsElement}
            algorithms={algorithms}
            MIN_COLUMNS={MIN_COLUMNS}
            MAX_COLUMNS={MAX_COLUMNS}
            resetGrid={this.resetGrid}
            cleanGrid={this.cleanGrid}
            visualizeAlgorithm={this.visualizeAlgorithm}
            numberColumns={numberColumns}
            handleOnChangeSlider={this.sizeAndCreateGrid}></AppControls>
        </header>

        <div id='Content' className='content'>
          <section className='side-bar'>
            <Tools 
              selectedTool={selectedTool}
              onToolChange={this.onToolChange}
              weightedGrid={weightedGrid}
              onWeightedGridChange={this.onWeightedGridChange}
              algoFinished={this.state.algoFinished}
              algoResults={this.state.algoResults}
            ></Tools>
          </section>

          <div id='GridContainer' className='grid-container'>
            <Grid
              debug_level={DEBUG_LEVEL}
              grid={grid}
              nodeSize={nodeSize}
              mouseIsPressed={mouseIsPressed}
              handleMouseDown={this.handleMouseDownGrid}
              handleMouseEnter={this.handleMouseEnter}
              handleMouseUp={this.handleMouseUp}
              selectedTool={selectedTool}
              weighted={weightedGrid}></Grid>
          </div>
        </div>
      </React.Fragment>
    )
  }

  changeNodeParams(row, col) {
    const { grid, selectedTool } = this.state
    switch (selectedTool) {
      case wallTool:
        grid[row][col].isWall = true
        document.getElementById(`node-${row}-${col}`).className = 'node node-wall'
        if (this.state.weightedGrid && document.getElementById(`input-node-${row}-${col}`)) {
          document.getElementById(`input-node-${row}-${col}`).classList.add('hidden')
        }
        break
      case cleanWallTool:
        grid[row][col].isWall = false
        document.getElementById(`node-${row}-${col}`).className = 'node node-clean'
        if (this.state.weightedGrid && document.getElementById(`input-node-${row}-${col}`)) {
          document.getElementById(`input-node-${row}-${col}`).classList.remove('hidden')
        }
        break
      default:
        break
    }
  }

  handleMouseDownGrid(e, row, col) {
    e.preventDefault()
    if (running) return

    //console.log('Mouse down Grid! row: ' + row + ', col: ' + col)
    const { startNode, endNode } = this.state
    mouseIsPressed = true
    if (row === startNode.row && col === startNode.col) {
      movingStart = true
    } else if (row === endNode.row && col === endNode.col) {
      movingEnd = true
    } else {
      this.changeNodeParams(row, col)
    }
  }

  handleMouseEnter(e, row, col) {
    e.preventDefault()
    if (running || !mouseIsPressed) return

    //console.log('Mouse enter, row: ' + row + ', col: ' + col)
    const { grid } = this.state

    if (movingStart) {
      // Remove prev start node
      const prevStartNode = this.state.startNode
      grid[prevStartNode.row][prevStartNode.col].isStart = false
      document.getElementById(`node-${prevStartNode.row}-${prevStartNode.col}`).className = 'node'

      // Configure new start node
      grid[row][col].isStart = true
      document.getElementById(`node-${row}-${col}`).className = 'node node-start'
      this.setState({ startNode: grid[row][col] })
    } else if (movingEnd) {
      // Remove prev end node
      const prevEndNode = this.state.endNode
      grid[prevEndNode.row][prevEndNode.col].isEnd = false
      document.getElementById(`node-${prevEndNode.row}-${prevEndNode.col}`).className = 'node'

      // Configure new end node
      grid[row][col].isEnd = true
      document.getElementById(`node-${row}-${col}`).className = 'node node-end'
      this.setState({ endNode: grid[row][col] })
    } else {
      this.changeNodeParams(row, col)
    }
  }

  handleMouseUp() {
    //console.log('Mouse up')
    mouseIsPressed = false
    movingStart = false
    movingEnd = false
    this.setState({ mouseIsPressed: mouseIsPressed })
  }

  sizeAndCreateGrid = (_numberColumns) => {
    if (running || !_numberColumns) return
    // window width - sidebar width - gridContainer margin*2 - gridContainer border*2
    const gridWidth = window.innerWidth - 170 - 40 - 20
    // window height - header height - gridContainer margin*2 - gridContainer border*2
    const gridHeight = window.innerHeight - 150 - 40 - 20
    const nodeSize = gridWidth / _numberColumns
    const rows = Math.floor(gridHeight / nodeSize)
    const cols = _numberColumns
    const startNodeRow = Math.floor(rows / 2)
    const startNodeCol = Math.floor(cols * 0.2)
    const endNodeRow = Math.floor(rows / 2)
    const endNodeCol = Math.floor(cols * 0.7)
    const initialGrid = getInitialGrid(rows, cols, startNodeRow, startNodeCol, endNodeRow, endNodeCol)
    this.setState({
      grid: initialGrid,
      startNode: initialGrid[startNodeRow][startNodeCol],
      endNode: initialGrid[endNodeRow][endNodeCol],
      nodeSize: nodeSize,
      numberColumns: _numberColumns,
    })
  }
}

const getInitialGrid = (rows, cols, startNodeRow, startNodeCol, endNodeRow, endNodeCol) => {
  const grid = []
  for (let row = 0; row < rows; row++) {
    const currentRow = []
    for (let col = 0; col < cols; col++) {
      currentRow.push(createNode(col, row, startNodeRow, startNodeCol, endNodeRow, endNodeCol))
    }
    grid.push(currentRow)
  }

  return grid
}

const createNode = (col, row, startNodeRow, startNodeCol, endNodeRow, endNodeCol) => {
  return {
    col,
    row,
    isStart: row === startNodeRow && col === startNodeCol,
    isEnd: row === endNodeRow && col === endNodeCol,
    distance: Infinity,
    isVisited: false,
    isWall: false,
    parentNode: null,
    weight: 0
  }
}
