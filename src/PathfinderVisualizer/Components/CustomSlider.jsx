import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Slider from '@material-ui/core/Slider'
import Tooltip from '@material-ui/core/Tooltip'

function ValueLabelComponent(props) {
  const { children, open, value } = props

  return (
    <Tooltip open={open} enterTouchDelay={0} placement='top' title={value}>
      {children}
    </Tooltip>
  )
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired,
}

const PrettoSlider = withStyles({
  root: {
    color: '#336B87',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '3px solid #90AFC5',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 3px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider)

export default class CustomSlider extends Component {

  handleChange(event, newValue) {
    this.props.handleOnChangeSlider(newValue)
  }

  render() {
    const { value, running, min, max } = this.props

    return (
      <div>
        <PrettoSlider valueLabelDisplay='auto' aria-label='pretto slider' value={value} step={running ? 0 : 2} min={min} max={max} onChange={this.handleChange.bind(this)} />
      </div>
    )
  }
}
