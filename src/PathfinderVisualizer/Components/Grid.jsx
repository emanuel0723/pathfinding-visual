import React, { Component } from 'react'
import Node from './Node'
import WeightModal from './WeightModal'

const WEIGHT_LIMIT = 100
let colorsWeighted = []

export default class Grid extends Component {
  constructor(props) {
    super(props)

    colorsWeighted = this.createWeightedColors()

    this.state = {}
  }

  render() {
    const {
      debug_level,
      grid,
      nodeSize,
      mouseIsPressed,
      handleMouseDown,
      handleMouseEnter,
      handleMouseUp,
      selectedTool,
      weighted,
    } = this.props

    return (
      <React.Fragment>
        {grid.map((row, rowIdx) => {
          return (
            <div
              id={`row-${rowIdx}`}
              key={rowIdx}
              className='row-div'
              style={{
                height: nodeSize + 'px',
              }}>
              {row.map((node, colIdx) => {
                const { isStart, isEnd, isWall } = node
                return (
                  <Node
                    debug_level={debug_level}
                    width={nodeSize}
                    height={nodeSize}
                    key={colIdx}
                    row={rowIdx}
                    col={colIdx}
                    isStart={isStart}
                    isEnd={isEnd}
                    isWall={isWall}
                    mouseIsPressed={mouseIsPressed}
                    onMouseDown={(e, row, col) => handleMouseDown(e, row, col)}
                    onMouseEnter={(e, row, col) => handleMouseEnter(e, row, col)}
                    onMouseUp={() => handleMouseUp()}
                    selectedTool={selectedTool}
                    weighted={weighted}
                    weight={grid[rowIdx][colIdx].weight}
                    weightLimit={WEIGHT_LIMIT}
                    colorsWeighted={colorsWeighted}
                    grid={grid}></Node>
                )
              })}
            </div>
          )
        })}
        {/*<WeightModal open={openWeightModal} row={weightCoords[0]} col={weightCoords[1]} grid={grid} handleSave={(e, row, col) => handleSaveWeightModal(e, row, col)} handleClose={handleCloseWeightModal}></WeightModal>*/}
      </React.Fragment>

    )

  }

  createWeightedColors() {
    const rgbLowest = [232, 245, 253];
    const rgbHighest = [47, 79, 79];
    var deltaRgb = { r: (rgbLowest[0]-rgbHighest[0])/WEIGHT_LIMIT, g: (rgbLowest[1]-rgbHighest[1])/WEIGHT_LIMIT, b: (rgbLowest[2]-rgbHighest[2])/WEIGHT_LIMIT };
    var colors = [];
    for (var i = 1; i <= WEIGHT_LIMIT+1; i++) {       
      colors.push({r:rgbHighest[0]+deltaRgb.r*i,g:rgbHighest[1]+deltaRgb.g*i,b:rgbHighest[2]+deltaRgb.b*i});
    }
    return colors;
  }


}