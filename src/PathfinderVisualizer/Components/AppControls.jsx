import React, { Component } from 'react'
//import Slider from 'react-rangeslider'
//import Slider from '@material-ui/core/Slider'
import CustomSlider from './CustomSlider'

import '../../styles/myStyle.css'

export default class AppControls extends Component {
  constructor(props) {
    super(props)

    this.state = {
      grid: [],
      running: false,
    }
  }

  render() {
    const { algorithms, MIN_COLUMNS, MAX_COLUMNS, resetGrid, cleanGrid, visualizeAlgorithm, numberColumns, handleOnChangeSlider } = this.props
    return (
      <React.Fragment>
        <div className='app-title'> Path Finding Visualizer</div>
        <div id='ControlsBar' className='controls-bar'>
          <div id='slider-div' className='slider-div'>
            <label className='label'>Number of columns</label>
            <CustomSlider value={numberColumns} handleOnChangeSlider={handleOnChangeSlider} min={MIN_COLUMNS} max={MAX_COLUMNS} running={this.state.running} />
          </div>
          <div id='algorithmBtns-div' className='algorithmBtns-div'>
            <button id='ResetBtn' className='btn-reset' onClick={() => resetGrid()}>
              Reset
            </button>
            <div className='vl'></div>
            <button id='CleanBtn' className='btn-clean btn-disabled' onClick={() => cleanGrid()}>
              Clean
            </button>
            {algorithms.map((algo, idx) => {
              return (
                <button id={'AlgoBtn_' + algo.name} key={idx} className='btn-algorithms' onClick={() => visualizeAlgorithm(algo)}>
                  {algo.description}
                </button>
              )
            })}
          </div>
        </div>
      </React.Fragment>
    )
  }

  updateControls(algorithm) {
    var cleanGridBtn = document.getElementById('CleanBtn')
    cleanGridBtn.disabled = algorithm == null
    if (algorithm == null) {
      cleanGridBtn.classList.add('btn-disabled')
    } else {
      cleanGridBtn.classList.remove('btn-disabled')
    }

    var algorithmBtns = document.getElementById('algorithmBtns-div').getElementsByTagName('*')
    for (var node of algorithmBtns) {
      if (node.id.includes('AlgoBtn')) {
        node.disabled = algorithm != null
        if (algorithm != null) {
          node.title = 'Reset to run again'
          node.classList.remove('btn-algorithms')
          if (node.id === 'AlgoBtn_' + algorithm) {
            node.classList.add('btn-algorithms-disabled')
          } else {
            node.classList.add('hidden')
          }
        } else {
          node.title = null
          node.classList.remove('hidden')
          node.classList.remove('btn-algorithms-disabled')
          node.classList.add('btn-algorithms')
        }
      }
    }
  }
}
