import React, { Component } from "react";
import { FaHandPointRight, FaDotCircle } from 'react-icons/fa';
import { cleanWallTool, weightTool } from './Tools'

import "../../styles/Node.css";

export default class Node extends Component {
  constructor(props) {
    super(props);
    this.state = { weight: this.props.weight, weightLimit: this.props.weightLimit, colors: this.props.colorsWeighted};
  }

  render() {
    const {
      debug_level,
      width,
      height,
      row,
      col,
      isStart,
      isEnd,
      isWall,
      onMouseDown,
      onMouseEnter,
      onMouseUp,
      selectedTool,
      weighted,
      weight,
      weightLimit
    } = this.props;
    const extraClassName = isEnd
      ? "node-end"
      : isStart
      ? "node-start"
      : isWall
      ? "node-wall"
      : "node-clean";

    const handleClicks = selectedTool !== weightTool || !weighted || isWall || isStart || isEnd

    return (
      <React.Fragment>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <div
          style={{
            width: width + "px",
            height: height + "px",
            fontSize: width + "px",
            cursor: this.getCursor()
          }}
          id={`node-${row}-${col}`}
          className={`node ${extraClassName}`}
          onMouseDown={ handleClicks? (e) => onMouseDown(e, row, col) : undefined}
          onMouseEnter={ handleClicks? (e) => onMouseEnter(e, row, col) : undefined}
          onMouseUp={ handleClicks? () => onMouseUp() : undefined}
        >{
          isStart ? <FaHandPointRight size={width*0.8}/> : 
          isEnd ? <FaDotCircle size={width*0.8}/> : 
          //weighted && !isWall ? `${weight}` :
          weighted && !isWall && !isStart && !isEnd ? this.inputField(row, col, width, height, selectedTool) :
          debug_level > 0 ? `${row}-${col}` : ''}</div>
      </React.Fragment>
    );
  }

  inputField(row, col, width, height, selectedTool) {
    return (
      <input
        id={`input-node-${row}-${col}`}
        className='input-weight'
        disabled={selectedTool != weightTool}
        onClick={(e) => {e.target.select()}}
        style={{
          width: (width-30) + "px",
          height: (height-30) + "px",
          cursor: this.getCursor()
        }}
        type="number"
        defaultValue={this.state.weight}
        onBlur={(e) => {this.setWeightValue(e, row, col)}}>

      </input>
    )
  }

  getCursor() {
    const {
      isStart,
      isEnd,
      isWall,
      selectedTool
    } = this.props;

    if (isStart || isEnd) {
      return 'drag'
    } else if (isWall) {
      return selectedTool === cleanWallTool ? 'pointer' : 'auto'
    } else {
      return selectedTool === weightTool ? 'copy' : selectedTool === cleanWallTool ? 'default' : 'pointer'
    }

  }

  setWeightValue(e, row, col) {
    const value = parseInt(e.target.value)
    const grid = this.props.grid
    if (value < 0 || Number.isNaN(value)) {
      e.target.value = 0
    } else if (value > this.state.weightLimit) {
      e.target.value = this.state.weightLimit
    }

    this.setState({ weight: e.target.value})
    grid[row][col].weight = parseInt(e.target.value)

    const color = this.state.colors[this.state.weightLimit - e.target.value]

    document.getElementById(`input-node-${row}-${col}`).style.background = 'rgb(' + color.r + ',' + color.g + ',' + color.b + ')'

  }

}

export const DEFAUL_NODE = {
  row: 0,
  col: 0,
};
