import React, { Component } from 'react'
import { RadioGroup, FormControlLabel, Radio } from '@material-ui/core'
import Checkbox from '@material-ui/core/Checkbox'
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn'
import CropFreeIcon from '@material-ui/icons/CropFree'
import ViewModuleIcon from '@material-ui/icons/ViewModule'
import GradientIcon from '@material-ui/icons/Gradient'

import '../../styles/myStyle.css'

export const wallTool = 'wallTool'
export const cleanWallTool = 'cleanWallTool'
export const weightTool = 'weightTool'

export default class Tools extends Component {
  constructor(props) {
    super(props)

    this.state = {
      grid: [],
      running: false,
    }
  }

  render() {
    const { selectedTool, onToolChange, weightedGrid, onWeightedGridChange, algoFinished, algoResults } = this.props

    return (
      <div className='tools-bar'>
        <div id='ToolsPanel' hidden={algoFinished}>
          <div>
            <h3>Tools</h3>
          </div>
          <RadioGroup aria-label='tool' name='tools' value={selectedTool} onChange={onToolChange}>
            <FormControlLabel className='tools-element' value='wallTool' control={<Radio checkedIcon={<ViewModuleIcon />} />} label='Wall' />
            <FormControlLabel className='tools-element' value='cleanWallTool' control={<Radio checkedIcon={<GradientIcon />} />} label='Clean Wall' />
            <FormControlLabel className='tools-element' value='weightTool' control={<Radio />} label='Weight' disabled={!weightedGrid} />
          </RadioGroup>

          <div className='pt-3'>
            <h3>Grid Type</h3>
          </div>
          <FormControlLabel
            className='tools-element'
            value='weightedGrid'
            control={<Checkbox icon={<CropFreeIcon />} checkedIcon={<MonetizationOnIcon />} name='checkedH' checked={weightedGrid} />}
            label='Weighted'
            onChange={onWeightedGridChange}
          />
        </div>
        <div id='StatisticsPanel' hidden={!algoFinished}>
          <div>
            <h3>Algo Statistics</h3>
          </div>
          <table className='pt-2'>
            <tbody>
              <tr>
                <th colSpan="2">{algoResults.algorithm}</th>
              </tr>
              {/* <tr>
                <td width='100px'>Total nodes</td>
                <td align='right'>{algoResults.totalNodes}</td>
              </tr>
              <tr>
                <td>Visited</td>
                <td align='right'>{algoResults.nodesVisited}</td>
              </tr> */}
              <tr>
                <td>Explored %</td>
                <td align='right'>{((algoResults.nodesVisited / algoResults.totalNodes) * 100).toFixed(0)}</td>
              </tr>
              <tr>
                <td>Efficiency %</td>
                <td align='right'>{(100 - (algoResults.nodesVisited / algoResults.totalNodes) * 100).toFixed(0)}</td>
              </tr>
              <tr>
                <td>Path length</td>
                <td align='right'>{algoResults.pathLength}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
