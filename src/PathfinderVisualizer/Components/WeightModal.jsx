import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

const WEIGHT_LIMIT = 100

export default class WeightModal extends Component {
  constructor(props) {
    super(props)

    this.state = { validForm: true }
  }

  render() {
    const { open, row, col, grid, handleSave, handleClose } = this.props

    return (
      <React.Fragment>
        <Dialog id='WeightModal' open={open} onClose={handleClose} aria-labelledby='form-dialog-title'>
          <DialogTitle id='form-dialog-title'>Set Node Weight</DialogTitle>
          <DialogContent>
            <DialogContentText>Row: {row}, Column: {col}.</DialogContentText>
            <TextField 
              autoFocus
              margin='dense' 
              id='WeightInput'
              error = {!this.state.validForm}
              helperText={'Range 0 - ' + WEIGHT_LIMIT}
              type='number' 
              onChange={this.numberInput.bind(this)} 
              defaultValue={grid[row] && grid[row][col] ? grid[row][col].weight : 0} >
            </TextField>
          </DialogContent>
          <DialogActions className='modal-buttons' style={{'display': 'block'}}>
            <Button id='WeightCancelBtn' onClick={handleClose} color='primary'>
              Cancel
            </Button>
            <Button id='WeightOkBtn' disabled={!this.state.validForm} onClick={(e) => handleSave(e, row, col)} color='primary'>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }

  numberInput(e) {
    let value = parseInt(e.target.value)
    this.setState({ validForm: 0 <= value && value <= WEIGHT_LIMIT })
  }
}
