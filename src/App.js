//import logo from './logo.svg';
import './styles/myStyle.css'
import PathfinderVisualizer from './PathfinderVisualizer/PathfinderVisualizer'

function App() {
  return (
    <div className='App'>
        <PathfinderVisualizer></PathfinderVisualizer>
    </div>
  )
}

export default App
