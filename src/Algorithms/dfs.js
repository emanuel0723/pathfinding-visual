const directions = [
  [1, 0],
  [0, 1],
  [-1, 0],
  [0, -1],
]

function helper(grid, i, j, end, orderedVisit) {
  let m = grid.length
  let n = grid[0].length
  if (i === end.row && j === end.col) {
    orderedVisit.push(grid[i][j])
    return true
  }

  orderedVisit.push(grid[i][j])

  //directions.forEach(function (dir) {
  for (let dirIdx = 0; dirIdx < 4; dirIdx++) {
    let dir = directions[dirIdx]
    let new_row = i + dir[0]
    let new_col = j + dir[1]

    if (0 <= new_row && new_row < m && 0 <= new_col && new_col < n && grid[new_row][new_col].isVisited === false && grid[new_row][new_col].isWall === false) {
      grid[new_row][new_col].isVisited = true
      grid[new_row][new_col].parentNode = grid[i][j]
      if (helper(grid, new_row, new_col, end, orderedVisit)) {
        return true
      }
    }
  }

  return false
}

export function dfs(grid, start, end) {
  let orderedVisit = []

  grid[start.row][start.col].isVisited = true
  helper(grid, start.row, start.col, end, orderedVisit)
  return orderedVisit
}
