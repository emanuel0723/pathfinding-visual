const directions = [
  [1, 0],
  [0, 1],
  [-1, 0],
  [0, -1],
]

export function bfs(grid, start, end) {
  let q = [[start, 0]]
  grid[start.row][start.col].isVisited = true
  let orderedVisit = []
  let m = grid.length
  let n = grid[0].length

  while (q.length > 0) {
    let element = q.shift()
    let currNode = element[0]
    let level = element[1]

    orderedVisit.push(grid[currNode.row][currNode.col])

    if (currNode.row === end.row && currNode.col === end.col) {
      return orderedVisit
    }

    directions.forEach(function (dir) {
      let new_row = currNode.row + dir[0]
      let new_col = currNode.col + dir[1]

      if (0 <= new_row && new_row < m && 0 <= new_col && new_col < n && grid[new_row][new_col].isVisited === false && grid[new_row][new_col].isWall === false) {
        grid[new_row][new_col].isVisited = true
        grid[new_row][new_col].parentNode = currNode
        q.push([grid[new_row][new_col], level + 1])
      }
    })
  }
  return orderedVisit
}
