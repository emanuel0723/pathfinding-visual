const directions = [
  [1, 0],
  [0, 1],
  [-1, 0],
  [0, -1],
]

const START_EXPLORER = 'start'
const END_EXPLORER = 'end'

export function bibfs(grid, start, end) {
  let q = [[start, 0, START_EXPLORER]]
  q.push([end, 0, END_EXPLORER])
  grid[start.row][start.col].isVisited = START_EXPLORER
  grid[end.row][end.col].isVisited = END_EXPLORER
  let orderedVisit = []
  let m = grid.length
  let n = grid[0].length
  let pathFound = false
  let collision1
  let collision2

  while (q.length > 0 && pathFound === false) {
    let element = q.shift()
    let currNode = element[0]
    let level = element[1]
    let explorer = element[2]

    for (let i = 0; i < directions.length; i++) {
      let dir = directions[i]
      let new_row = currNode.row + dir[0]
      let new_col = currNode.col + dir[1]

      if (0 <= new_row && new_row < m && 0 <= new_col && new_col < n && grid[new_row][new_col].isWall === false) {
        if (grid[new_row][new_col].isVisited === false) {
          grid[new_row][new_col].parentNode = currNode
          grid[new_row][new_col].isVisited = explorer
          orderedVisit.push(grid[new_row][new_col])
          q.push([grid[new_row][new_col], level + 1, explorer])
        } else if (grid[new_row][new_col].isVisited !== currNode.isVisited) {
          collision1 = currNode
          collision2 = grid[new_row][new_col]
          pathFound = true
        }
      }
    }
  }
  return [orderedVisit, collision1, collision2]
}
