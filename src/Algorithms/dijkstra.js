const directions = [
  [1, 0],
  [0, 1],
  [-1, 0],
  [0, -1],
]

var Heap = require("heap");

export function dijkstra(grid, start, end) {
  var heap = new Heap(function (a, b) {
      if (a[0] == b[0]) {
        return a[1] - b[1]
      }
      return a[0] - b[0]
  });

  heap.push([0, 0, start])
  grid[start.row][start.col].isVisited = true
  let orderedVisit = []
  let m = grid.length
  let n = grid[0].length

  while (heap.size() > 0) {
    let element = heap.pop()
    let weight = element[0]
    let level = element[1]
    let currNode = element[2]
    

    orderedVisit.push(grid[currNode.row][currNode.col])

    if (currNode.row === end.row && currNode.col === end.col) {
      return orderedVisit
    }

    directions.forEach(function (dir) {
      let new_row = currNode.row + dir[0]
      let new_col = currNode.col + dir[1]

      if (0 <= new_row && new_row < m && 0 <= new_col && new_col < n && grid[new_row][new_col].isVisited === false && grid[new_row][new_col].isWall === false) {
        grid[new_row][new_col].isVisited = true
        grid[new_row][new_col].parentNode = currNode
        heap.push([weight + grid[new_row][new_col].weight, level + 1, grid[new_row][new_col]])
      }
    })
  }
  return orderedVisit
}
